import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
   MultiplierPipe
} from './multiplier.pipe'

import { 
   ChildComponent 
} from './child.component';  

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,MultiplierPipe,ChildComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
