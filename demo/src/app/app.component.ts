import { Component } from '@angular/core';
import { appService } from './app.service';

 @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [appService]
})
export class AppComponent {
	value : string[];
	appTitle:string = "";
	values = "";
	Status:boolean = true;
	clickMessage = "Hello";
  constructor(private _appservice :appService){}

clicked(event){
	this.Status = false;
	}
	clickMe(){
	this.clickMessage = "How are you ?";
	}
  ngOnInit():void{
  		this.appTitle = "This is Angular App";
  		this.value  = this._appservice.getApp();
  		
  }

  ngAfterContentInit ():void{
  this.values = "Hello";
  }

  }
