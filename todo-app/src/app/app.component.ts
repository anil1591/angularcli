import { Component } from '@angular/core';
import { ToDoDataService } from './todo-data.service';
import { ToDo } from './to-do';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ToDoDataService]
})
export class AppComponent {

	newTodo:ToDo = new ToDo(); 
  private todoDataService: ToDoDataService;
  constructor(todoDataService: ToDoDataService){

  this.todoDataService = todoDataService;
  }
 
  addTodo(){
  this.todoDataService.addToDo(this.newTodo);
  this.newTodo = new ToDo();
  }

  toggleToDoComplete(todo){
  	this.todoDataService.toggleToDoComplete(todo);
  }

  removeTodo(todo){
  	this.todoDataService.deleteToDoById(todo.id);
  }

   get todos() {
    return this.todoDataService.getAllTodos();
  }

}
